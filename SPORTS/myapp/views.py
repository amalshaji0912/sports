from unicodedata import name
from urllib import request
from django.shortcuts import render, redirect

from .models import *
from django.contrib.auth import authenticate,login,logout
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
def add_users(request):
    if request.method=='POST':
        f=request.POST.get('full_name')
        a=request.POST.get('birthday')
        g=request.POST.get('gender')
        e=request.POST.get('email')
        ph=request.POST.get('phone')
        us=request.POST.get('usname')
        psw=request.POST.get('pssword')
        User.objects.create_user(username=us,name=f,age=a,ge=g,email=e,phone=ph,psswd=psw)
        return render(request,"user/login.html")
    return render(request,"user/signup.html")

def login_user(request):
    if request.method=='POST':
        em=request.POST.get('email')
        pw=request.POST.get('password')
        user=authenticate(request,email=em,psswd=pw)
        if User:
            login(request,user)
            return render(request,"user/index.html")
        else:
            return HttpResponse("Invalid User!!")
    return render(request,"user/login.html")

def home(request):
    p1=product.objects.all()

    return render(request,"user/index.html",{"data":p1})

def stock(request):
    return render(request,"user/collection.html")

def admin1(request):
    return render(request,"dashboard/index1.html")
def admin2(request):
    return render(request,"dashboard/index2.html")
   
def admin3(request):
    return render(request,"dashboard/index3.html") 
    

def addc(request):
    if request.method=='POST':
        c=request.POST.get('categoryname')
        category.objects.create(cname=c)
    return render(request,"dashboard/cat.html")       

def addpr(request):
  return render(request,"dashboard/pro.html")

def addproduct(request):
    k=category.objects.all()
    if request.method=='POST':
        p=request.POST.get('productname')
        c=request.POST.get('category')
        pr=request.POST.get('price')
        im=request.FILES['image']
        s=request.POST.get('stock')
        st=request.POST.get('stockstatus')
        d=request.POST.get('description')
        c1=category.objects.get(id=c)
        product.objects.create(name=p,category=c1,price=pr,image=im,stock=s,stockstatus=st,description=d)
    return render(request,"dashboard/pro.html",{"data":k})

def del_product(request,pid):
    x=product.objects.get(id=pid)
    x.delete()
    return redirect("viewproduct")

def update_product(request,pid):
    x=product.objects.filter(id=pid).values()
    ca=category.objects.all()
    if request.method=="POST":
        F=request.POST.get('productname')
        c=request.POST.get('category')
        G=request.POST.get('price')
        im=request.FILES['image']
        fo=FileSystemStorage()
        im2=fo.save(im.name,im)
        B=request.POST.get('stock')
        M=request.POST.get('description')
        
        x.update(name=F,category=c,price=G,image=im2,stock=B,description=M)
        return redirect("viewproduct")
    return render(request,"dashboard/update.html",{"productdata":x[0],"id":pid,"cat":ca})


def viewproducts(request):
    p=product.objects.all()
    return render(request,"dashboard/table.html",{'data':p})

def vieworder(request):
    return render(request,"dashboard/order.html")

    
def admin_login(request):
    if request.method=='POST':
        e=request.POST.get('email')
        p=request.POST.get('password')
        user=authenticate(request,email="admin@gmail.com",password='123')
        if user:
            login(request,user)
            return redirect('dashboard')
    return render(request,"adminlogin/index.html")

#admin logout
def admin_logout(request):
    logout(request)
    return redirect('adminlogin')    

def usercart(request):
    return render(request,"user/cart.html")    