

from django.urls import path

from myapp import views
from .views import *

urlpatterns = [

   path('',views.home,name="home"),
   path('coll',views.stock,name="coll"),
   path('sign',views.add_users,name="add"),
   path('log',views.login_user,name="login"),
   path('a1',views.admin1,name="ad1"),
   path('ac',views.addc,name="adc"),
   path('ap',views.addpr,name="adp"),
   path('adp',views.addproduct,name="addp"),
   path('del<int:pid>',views.del_product,name="dl"),
   path('edit<int:pid>',views.update_product,name="ed"),
   path('get',views.viewproducts,name='viewproduct'),
   path('adminlogin',views.admin_login,name='adminlogin'),
   path('adminlogout',views.admin_logout,name='logout'),
   path('vieworder',views.vieworder,name="vieworder"),
   path('cart',views.usercart,name="cartproduct")

]


addproduct