from email.mime import image
from pyexpat import model
from django.db import models

from django.contrib.auth.models import AbstractUser,BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def _create_user(self, email, password, **other_fields):
        """
        Create and save a user with the given email and password. And any other fields, if specified.
        """
        if not email:
            raise ValueError('An Email address must be set')
        email = self.normalize_email(email)
        
        user = self.model(email=email, **other_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **other_fields):
        other_fields.setdefault('is_staff', False)
        other_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **other_fields)

    def create_superuser(self, email, password=None, **other_fields):
        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)

        if other_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if other_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **other_fields)
    

class User(AbstractUser):
    username=models.CharField(max_length=100,null=True,blank=True)
    name=models.CharField(max_length=100,null=True,blank=True)
    email = models.EmailField(null=True,max_length=255, unique=True)
    age=models.DateField(null=True)
    phone = models.IntegerField(null=True)
    ge=models.CharField(choices=(('Male','Male'),('Female','Female')),null=True,max_length=30)
    psswd=models.CharField(max_length=12,null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['psswd']
    objects=UserManager()

    def get_username(self):
        return self.email 
    
class category(models.Model):
    cname=models.CharField(max_length=40,null=True)
    
    def __str__(self):
        return self.cname
   
    
class product(models.Model):
    name=models.CharField(max_length=40,null=True)
    category=models.ForeignKey('category', on_delete=models.CASCADE,null=True)
    price=models.IntegerField(null=True)
    image=models.ImageField(upload_to='image/',null=True)
    stock=models.IntegerField(null=True)
    stockstatus=models.BooleanField()
    description=models.CharField(max_length=60,null=True)
    
    def __str__(self):
        return self.name
   